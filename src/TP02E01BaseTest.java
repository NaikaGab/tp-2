import static org.junit.Assert.*;

import org.junit.Test;

public class TP02E01BaseTest {

    @Test
    public void ExtraitValeurstest() {

        // Valeurs paires

        int[] tab1 = { 7, 5, 8, 4, 4, 3, 1, 2, 6 };
        int[] tabPaire = TP02E01Base.extraitValeurs(tab1, true);
        int[] tabReponse = { 8, 4, 4, 2, 6 };

        assertArrayEquals(tabReponse, tabPaire);

        // Valeurs impaires (avec tab1)

        int[] tabImpaire = TP02E01Base.extraitValeurs(tab1, false);
        int[] tabRep = { 7, 5, 3, 1 };

        assertArrayEquals(tabRep, tabImpaire);

        // Valeurs paires en disant qu'elles sont impaires

        int[] tab2 = { 2, 6, 4, 2, 8, 6, 4, 10, 8 };
        int[] tabImpaire2 = TP02E01Base.extraitValeurs(tab2, false);
        int[] tabReponse2 = {};

        assertArrayEquals(tabReponse2, tabImpaire2);
    }

    @Test
    public void triSelTest() {

        // Valeurs positives

        int[] tab1 = { 3, 5, 2, 7, 1, 4, 8, 6, 9 };
        int[] tabRep = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        TP02E01Base.triSel(tab1);
        assertArrayEquals(tabRep, tab1);

        int[] tab2 = { 33, 55, 22, 77, 11, 44, 88, 66, 99 };
        int[] tabRep2 = { 11, 22, 33, 44, 55, 66, 77, 88, 99 };

        TP02E01Base.triSel(tab2);
        assertArrayEquals(tabRep2, tab2);

        // Valeurs de 0

        int[] tab3 = { 0, 0, 0, 0, 0, 0, 0 };
        int[] tabRep3 = { 0, 0, 0, 0, 0, 0, 0 };

        TP02E01Base.triSel(tab3);
        assertArrayEquals(tabRep3, tab3);

        // Valeurs équivalentes

        int[] tab4 = { 9, 9, 9, 9, 9, 9, 9 };
        int[] tabRep4 = { 9, 9, 9, 9, 9, 9, 9 };

        TP02E01Base.triSel(tab4);
        assertArrayEquals(tabRep4, tab4);
    }
}