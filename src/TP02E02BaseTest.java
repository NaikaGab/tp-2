import static org.junit.Assert.*;

import org.junit.Test;

public class TP02E02BaseTest {

    @Test
    public void nbFinSemainesTest() {

        int[][] calendrier = TP02E02Base.genereMois(4, 2009);
        assertEquals(4, TP02E02Base.nbFinSemaines(calendrier));

        int[][] calendrier2 = TP02E02Base.genereMois(6, 2018);
        assertEquals(4, TP02E02Base.nbFinSemaines(calendrier));

        int[][] calendrier3 = TP02E02Base.genereMois(9, 1999);
        assertEquals(4, TP02E02Base.nbFinSemaines(calendrier3));


    }

    @Test
    public void trouveJourTest() {

        int[][] calendrier = TP02E02Base.genereMois(4, 2020);
        assertEquals(18, TP02E02Base.trouveJour(calendrier, 6, -2));

        int[][] calendrier2 = TP02E02Base.genereMois(5, 2020);
        assertEquals(16, TP02E02Base.trouveJour(calendrier2, 6, -3));

        int[][] calendrier3 = TP02E02Base.genereMois(7, 2019);
        assertEquals(16, TP02E02Base.trouveJour(calendrier2, 6, -3));
    }
}